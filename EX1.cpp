#include <iostream>
#include "sqlite3.h"

using namespace std;

int main()
{
	sqlite3* connection = nullptr;

	int result = sqlite3_open("FirstPart.db", &connection);

	if (SQLITE_OK != result)
	{
		cout << "Cant Open SQLITE3" << endl;
	}
	cout << "SQLITE3 Opend Seccesfuly" << endl;

	sqlite3_stmt* query = nullptr;

	result = sqlite3_prepare_v2(connection, "CREATE TABLE people(id integer PRIMARY KEY, name text);", -1, &query, nullptr);

	while (SQLITE_ROW == sqlite3_step(query))
	{
		cout << sqlite3_column_text(query, 0) << endl;
	}

	result = sqlite3_prepare_v2(connection, "INSERT INTO people VALUES (yosi, niv, nir);", -1, &query, nullptr);

	while (SQLITE_ROW == sqlite3_step(query))
	{
		cout << sqlite3_column_text(query, 0) << endl;
	}

	result = sqlite3_prepare_v2(connection, "UPDATE people SET name = miii WHEN id = 3;", -1, &query, nullptr);

	while (SQLITE_ROW == sqlite3_step(query))
	{
		cout << sqlite3_column_text(query, 0) << endl;
	}

	sqlite3_finalize(query);
	sqlite3_close(connection);
	system("PAUSE");
	return 0;
}